package ro.pub.cs.systems.eim.exemplu_colocviu2

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import ro.pub.cs.systems.eim.exemplu_colocviu2.Constants.COUNTER
import ro.pub.cs.systems.eim.exemplu_colocviu2.Constants.PRESSED_BUTTONS
import ro.pub.cs.systems.eim.exemplu_colocviu2.ui.theme.Exemplu_colocviu2Theme

class SecondActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val counter = intent.getIntExtra(COUNTER, 0)
        val pressedButtons = intent.getStringExtra(PRESSED_BUTTONS)
        setContent {
            Column{
                Spacer(modifier = Modifier.padding(16.dp))
                OutlinedTextField(value = "$counter pressed buttons:  $pressedButtons", onValueChange = {}, modifier = Modifier.align(
                    Alignment.CenterHorizontally))
                Spacer(modifier = Modifier.padding(16.dp))
                Row {
                    Button(
                        onClick = {
                            setResult(RESULT_OK)
                            finish()
                                  },
                        modifier = Modifier.weight(1f).padding(8.dp)
                    )
                    {
                        Text("Register")
                    }
                    Button(
                        onClick = {
                            setResult(RESULT_CANCELED)
                            finish()
                                  },
                        modifier = Modifier.weight(1f).padding(8.dp)

                    )
                    {
                        Text("Cancel")
                    }

                }

            }
        }
    }
}
