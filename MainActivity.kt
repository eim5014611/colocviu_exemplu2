package ro.pub.cs.systems.eim.exemplu_colocviu2

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.BitmapFactory.Options
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat.startActivity
import androidx.core.os.bundleOf
import ro.pub.cs.systems.eim.exemplu_colocviu2.Constants.COUNTER
import ro.pub.cs.systems.eim.exemplu_colocviu2.Constants.PRESSED_BUTTONS
import ro.pub.cs.systems.eim.exemplu_colocviu2.ui.theme.Exemplu_colocviu2Theme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Colocviu_2_UI()
        }
    }
}

@SuppressLint("UnrememberedMutableState")
@Composable
fun Colocviu_2_UI () {
    val context = LocalContext.current

    val pressedButtons = rememberSaveable { mutableStateOf("") }
    //var counter = 0
    var counter = rememberSaveable { mutableIntStateOf(0)}

    val activityResultsLauncher = rememberLauncherForActivityResult(ActivityResultContracts.StartActivityForResult())
    {result ->
        if (result.resultCode == Activity.RESULT_OK) {
            Toast.makeText(context, "The activity returned with result OK", Toast.LENGTH_LONG).show()
            counter.intValue = 0
            pressedButtons.value = ""
        }
        else if (result.resultCode == Activity.RESULT_CANCELED) {
            Toast.makeText(context, "The activity returned with result CANCELED", Toast.LENGTH_LONG).show()
            counter.intValue = 0
            pressedButtons.value = ""
        }
    }


    Column(modifier = Modifier.fillMaxWidth()){

        Spacer(modifier = Modifier.padding(16.dp))

        Button(
            onClick = {pressedButtons.value += "N "
                      counter.value += 1},
            modifier = Modifier.align(Alignment.CenterHorizontally)
           )
        {
            Text("N")
        }

        Row(modifier = Modifier.fillMaxWidth()){
            Button(
                onClick = {pressedButtons.value += "V "
                            counter.value += 1},
                modifier = Modifier
                    .wrapContentSize()
                    .weight(1f)
                    .padding(16.dp))
            {
                Text("V")
            }
            Button(
                onClick = {pressedButtons.value += "E "
                          counter.value += 1},
                modifier = Modifier
                    .wrapContentSize()
                    .weight(1f)
                    .padding(16.dp))
            {
                Text("E")
            }
        }

        Button(
            onClick = {pressedButtons.value += "S "
                        counter.value += 1},
            modifier = Modifier.align(Alignment.CenterHorizontally))
        {
            Text("S")
        }

        Spacer(modifier = Modifier.padding(16.dp))

        OutlinedTextField(value = "${counter.value} pressed buttons:  ${pressedButtons.value}", onValueChange = {}, modifier = Modifier.align(Alignment.CenterHorizontally))

        Spacer(modifier = Modifier.padding(16.dp))

        Button(
            onClick = {
                val intent = Intent(context, SecondActivity::class.java)
                intent.putExtra(COUNTER, counter.intValue)
                intent.putExtra(PRESSED_BUTTONS, pressedButtons.value)
                activityResultsLauncher.launch(intent)
            },
            modifier = Modifier.align(Alignment.CenterHorizontally))
        {
            Text("Next")
        }

    }
    Log.d("counter", "${counter.intValue}")

}

